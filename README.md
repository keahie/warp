# README #

[Deutsch]
Nur ein kleines Warp Plugin f�r Spigot (Minecraft) 1.8.8, welches ich in k�rzester Zeit erstellt habe. Ich werde daran (vorrausichtlich) nicht weiter arbeiten oder �hnliches. Wer will kann es selber verwenden um darauf ein eigenes Warp Plugin aufzubauen.

[English]
This is just a little warp plugin for Spigot (Minecraft) 1.8.8, which I made in a very short time. I will not support this plugin in the future but you can use it to make your own warp plugin on top of it.