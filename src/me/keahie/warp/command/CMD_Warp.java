package me.keahie.warp.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.keahie.warp.Warp;

/**
 * 
 * @author keahie
 *
 * Just a simple project to show some java and oo skills in combination with the spigot api
 *
 */

public class CMD_Warp implements CommandExecutor
{
	private Warp plugin;
	
	public CMD_Warp(Warp plugin)
	{
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args)
	{
		if (!(sender instanceof Player))
		{
			System.out.println("Du musst ein Spieler sein um diesen Command ausführen zu können!");
			return true;
		}
		
		Player p = (Player) sender;
		
		if (args.length == 1)
		{
			if (p.hasPermission("warp.teleport"))
			{
				p.teleport(this.plugin.getWarpManager().getLocationOfWarp(args[0]));
				p.sendMessage("§aDu wurdest zu §6" + args[0] + " §ateleportiert");
			}
			else
			{
				p.sendMessage("§cDu hast keine Rechte dafür");
			}
		}
		return true;
	}

}
