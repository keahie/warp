package me.keahie.warp;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * 
 * @author keahie
 *
 * Just a simple project to show some java and oo skills in combination with the spigot api
 *
 */

public class WarpManager
{
	private Warp plugin;
	private FileConfiguration cfg;
	
	public WarpManager(Warp plugin)
	{
		this.plugin = plugin;
		this.cfg = this.plugin.getFileConfiguration();
	}
	
	public boolean warpExists(String warp)
	{
		return plugin.getFileConfiguration().getString("Warp." + warp) != null;
	}
	
	public void createWarp(String name, Player p)
	{
		Location loc = p.getLocation();
		double x = loc.getX();
		double y = loc.getY();
		double z = loc.getZ();
		float yaw = loc.getYaw();
		float pitch = loc.getPitch();
		String world = loc.getWorld().getName();
		
		String path = "Warp." + name + ".";
		
		cfg.set(path + ".X", x);
		cfg.set(path + ".Y", y);
		cfg.set(path + ".Z", z);
		cfg.set(path + ".YAW", yaw);
		cfg.set(path + ".PITCH", pitch);
		cfg.set(path + ".WORLD", world);
		
		this.plugin.saveWarps();
	}
	
	public void deleteWarp(String name)
	{
		cfg.set("Warp." + name, null);
		this.plugin.saveWarps();
	}
	
	public Location getLocationOfWarp(String name)
	{
		String path = "Warp." + name + ".";
		
		double x = cfg.getDouble(path + ".X");
		double y = cfg.getDouble(path + ".Y");
		double z = cfg.getDouble(path + ".Z");
		float yaw = (float) cfg.getDouble(path + ".YAW");
		float pitch = (float) cfg.getDouble(path + ".PITCH");
		World world = plugin.getServer().getWorld(cfg.getString(path + ".WORLD"));
		
		return new Location(world, x, y, z, yaw, pitch);
	}
}
