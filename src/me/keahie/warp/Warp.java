package me.keahie.warp;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import me.keahie.warp.command.CMD_Delwarp;
import me.keahie.warp.command.CMD_Setwarp;
import me.keahie.warp.command.CMD_Warp;

/**
 * 
 * @author keahie
 *
 * Just a simple project to show some java and oo skills in combination with the spigot api
 *
 */

public class Warp extends JavaPlugin
{
	private File warps;
	private FileConfiguration yml;
	
	private WarpManager warpManager;
	
	@Override
	public void onEnable()
	{
		this.initConfigFile();
		this.registerCommands();
		this.warpManager = new WarpManager(this);
	}
	
	private void initConfigFile()
	{
		warps = new File(this.getDataFolder(), "warps.yml");
		if (!warps.exists())
		{
			this.saveDefaultConfig();
		}
		this.yml = new YamlConfiguration();
	}
	
	private void registerCommands()
	{
		this.getCommand("warp").setExecutor(new CMD_Warp(this));
		this.getCommand("setwarp").setExecutor(new CMD_Setwarp(this));
		this.getCommand("delwarp").setExecutor(new CMD_Delwarp(this));
	}
	
	public void saveWarps()
	{
		try
		{
			this.yml.save(this.warps);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getFileConfiguration()
	{
		return this.yml;
	}
	
	public WarpManager getWarpManager()
	{
		return warpManager;
	}
}
